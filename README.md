Simple console application for downloading website content.
The project was built based on the actor model approach using the Akka.NET library. It allows for the creation and maintains the structure of actors, where every actor works in his thread. Such an approach helps to avoid concurrency and deadlock issues.
First of all, the application creates a website map with a directory structure using a recursive parallel loop. It includes pages and connections between them. Then it creates an actor for every page, which downloads it.

Installation and use.
1. Download the repository files.
2. Build an application
    2.1 Using any IDE, such as Visual Studio or IntelliJ Rider
    2.2 With the command line tool, using dotnet build command
3. Launch HtmlLoader.exe in ..\htmlloader\HtmlLoader\HtmlLoader\bin\Debug\net6.0\
4. Application will inform when downloading is done.
5. Downloaded website content you can find in the Download folder in the same directory.
