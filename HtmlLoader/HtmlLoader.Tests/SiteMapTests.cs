using HtmlLoader.Models;

namespace HtmlLoader.Tests
{
    public class SiteMapTests
    {
        private SiteMap _siteMap;

        private string[] _urls = new string[]
        {
            "https://tretton37.com/",
            "https://tretton37.com/who-we-are",
            "https://tretton37.com/what-we-do",
            "https://tretton37.com/knowledge-sharing",
            "https://tretton37.com/join"
        };

        private Tuple<string, string>[] _links = new Tuple<string, string>[]
        {
            new Tuple<string, string>("https://tretton37.com/", "https://tretton37.com/who-we-are"),
            new Tuple<string, string>("https://tretton37.com/", "https://tretton37.com/what-we-do"),
            new Tuple<string, string>("https://tretton37.com/who-we-are", "https://tretton37.com/join")
        };

        public SiteMapTests()
        {
            _siteMap = new SiteMap();

            foreach (var url in _urls)
            {
                _siteMap.AddPage(url);
            }

            foreach (var link in _links)
            {
                var result = _siteMap.AddLink(link);
            }
        }

        [Fact]
        public void Add_page_success_test()
        {
            var newUrl = "https://tretton37.com/contact";
            var result = _siteMap.AddPage(newUrl);
            result.Should().BeTrue();
        }

        [Fact]
        public void Add_link_success_test()
        {
            var item1 = _urls[0];
            var item2 = _urls[3];
            var newLink = new Tuple<string, string>(item1, item2);

            var result = _siteMap.AddLink(newLink);
            result.Should().BeTrue();
        }

        [Fact]
        public void Add_page_fail_test()
        {
            var existingUrl = _urls.First();
            var result = _siteMap.AddPage(existingUrl);
            result.Should().BeFalse();
        }

        [Fact]
        public void Add_link_not_exist_page_fail_test()
        {
            var item1 = _urls[0];
            var item2 = "https://tretton37.com/meet";
            var newLink = new Tuple<string, string>(item1, item2);

            var result = _siteMap.AddLink(newLink);
            result.Should().BeFalse();
        }

        [Fact]
        public void Add_existing_link_fail_test()
        {
            var item1 = _urls[0];
            var item2 = _urls[1];
            var newLink = new Tuple<string, string>(item1, item2);

            var result = _siteMap.AddLink(newLink);
            result.Should().BeFalse();
        }
    }
}