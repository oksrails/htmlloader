﻿using HtmlLoader.Utils;

namespace HtmlLoader.Tests
{
    public class DirectoryHelpersTests
    {
        private string _subdirectories;
        private string _basePath;

        public DirectoryHelpersTests()
        {
            _subdirectories = "/dir1/dir2/dir3";
            _basePath = TestHelpers.GetBasePath();
            Directory.Delete(_basePath, true);
        }

        [Fact]
        public void Create_directories_success_test()
        {
            var directoryInfo = new DirectoryInfo(_basePath);

            directoryInfo = DirectoryHelpers.CreateSubdirectories(directoryInfo, _subdirectories);

            var dirs = Directory.GetDirectories(_basePath, "*", SearchOption.AllDirectories);

            dirs.Should().HaveCount(3);
        }

        [Fact]
        public void Get_file_full_path_success_test()
        {
            string testFilePath = _basePath + "/Tests.html";

            var directoryInfo = new DirectoryInfo(_basePath);
            var filePath = DirectoryHelpers.BuildHtmlFilePath(directoryInfo);
            filePath.Should().Be(testFilePath);
        }
    }
}
