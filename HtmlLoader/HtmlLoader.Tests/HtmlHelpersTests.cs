﻿using HtmlLoader.Utils;

namespace HtmlLoader.Tests
{
    public class HtmlHelpersTests
    {
        [Fact]
        public void Validate_href_succes_result_test()
        {
            string[] hrefs = new string[]
            {
                "/blog",
                "/who-we-are",
                "/link1/link2"
            };

            foreach (string href in hrefs)
            {
                HtmlHelpers.ValidateHref(href).Should().BeTrue();
            }
        }

        [Fact]
        public void Validate_href_fail_result_test()
        {
            string[] hrefs = new string[]
            {
                "http://leetspeak.se",
                "mailto:vasb@gerggba37.pbz"
            };

            foreach (string href in hrefs)
            {
                HtmlHelpers.ValidateHref(href).Should().BeFalse();
            }
        }
    }
}
