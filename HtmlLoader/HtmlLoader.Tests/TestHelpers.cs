﻿using System.Reflection;

namespace HtmlLoader.Tests
{
    /// <summary>
    /// Utility class for directory tests
    /// </summary>
    public static class TestHelpers
    {
        /// <summary>
        /// Find base directory
        /// </summary>
        /// <returns></returns>
        public static string GetBasePath()
        {
            var relativePath = "Tests";
            var codeBaseUrl = new Uri(Assembly.GetExecutingAssembly().CodeBase);
            var codeBasePath = Uri.UnescapeDataString(codeBaseUrl.AbsolutePath);
            var dirPath = Path.GetDirectoryName(codeBasePath);
            var path = Path.Combine(dirPath, relativePath);
            Directory.CreateDirectory(path);
            return path;
        }
    }
}
