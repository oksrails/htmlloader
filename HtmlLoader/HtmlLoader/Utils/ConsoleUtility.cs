﻿namespace HtmlLoader.Utils
{
    /// <summary>
    /// Utility for progress output
    /// </summary>
    public static class ConsoleUtility
    {
        const char _block = '■';
        const string _back = "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b";
        public static void WriteProgressBar(int percent, bool update = false)
        {
            if (update)
                Console.Write(_back);
            Console.Write("[");
            var p = (int)(percent / 10f + .5f);
            for (var i = 0; i < 10; ++i)
            {
                if (i >= p)
                    Console.Write(' ');
                else
                    Console.Write(_block);
            }
            Console.Write("] {0,3:##0}%", percent);
        }

        public static void RefreshProgress(int total, int done)
        {
            int progress = (int)(done / total * 100);

            WriteProgressBar(progress, true);
        }
    }
}
