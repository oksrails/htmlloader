﻿namespace HtmlLoader.Utils
{
    /// <summary>
    /// Html heplers
    /// </summary>
    public static class HtmlHelpers
    {
        /// <summary>
        /// Check if href is valid for processing
        /// </summary>
        /// <param name="href">Href for validation</param>
        /// <returns>Validation result</returns>
        public static bool ValidateHref(string href)
        {
            return !href.StartsWith("https") & !href.StartsWith("http") & !href.Equals("/") & !href.Contains(":") & !href.Contains(".");
        }
    }
}