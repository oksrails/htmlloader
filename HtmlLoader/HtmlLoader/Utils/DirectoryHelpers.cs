﻿using System.Text;

namespace HtmlLoader.Utils
{
    /// <summary>
    /// Helpers using for creating directory tree and file names
    /// </summary>
    public static class DirectoryHelpers
    {
        /// <summary>
        /// Creates directory chain according to given path
        /// </summary>
        /// <param name="directoryInfo">Base directory info</param>
        /// <param name="path">A directory chain that needs to be created</param>
        /// <returns>Result directory info</returns>
        public static DirectoryInfo CreateSubdirectories(this DirectoryInfo directoryInfo, string path)
        {
            var subdirectories = path.Split('/');
            foreach (var subdirectory in subdirectories.Where(s => !string.IsNullOrEmpty(s)))
            {
                directoryInfo = directoryInfo.CreateSubdirectory(subdirectory);
            }

            return directoryInfo;
        }

        /// <summary>
        /// Creates file path based on directory info
        /// </summary>
        /// <param name="directoryInfo">Base directory info</param>
        /// <returns>Full file path</returns>
        public static string BuildHtmlFilePath(DirectoryInfo directoryInfo)
        {
            var pathBuilder = new StringBuilder(directoryInfo.FullName);
            pathBuilder.Append("/");
            pathBuilder.Append(directoryInfo.Name);
            pathBuilder.Append(".html");

            return pathBuilder.ToString();
        }
    }
}
