﻿using Akka.Actor;
using HtmlLoader.Actors;
using static HtmlLoader.Models.Messages;

Console.WriteLine("Type the url or press Enter to download default address.");

var baseUrl = Console.ReadLine();

if (string.IsNullOrEmpty(baseUrl))
    baseUrl = "https://tretton37.com/";

var baseDirectory = new DirectoryInfo(Environment.CurrentDirectory).CreateSubdirectory("Download");

var actorSystem = ActorSystem.Create("TraverseSystem");
var traverseActor = actorSystem.ActorOf(TraverseActor.GetProps(baseUrl, baseDirectory));
traverseActor.Tell(Start.Instance);

Console.ReadLine();
