﻿namespace HtmlLoader.Models
{
    /// <summary>
    /// Message models
    /// </summary>
    public record Messages
    {
        /// <summary>
        /// Start actor
        /// </summary>
        public record Start
        {
            public static Start Instance = new Start();
        }

        /// <summary>
        /// Actor work done
        /// </summary>
        public record Done
        {
            public string Url { get; init; }

            public Done(string url)
            {
                Url = url;
            }
        }
    }
}
