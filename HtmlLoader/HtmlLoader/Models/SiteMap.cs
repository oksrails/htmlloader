﻿using Akka.Util;
using System.Collections.Concurrent;

namespace HtmlLoader.Models
{
    /// <summary>
    /// Map of the site with pages and links between them
    /// </summary>
    public class SiteMap
    {
        public ConcurrentDictionary<string, ConcurrentSet<string>> AdjacencyList { get; } = new ConcurrentDictionary<string, ConcurrentSet<string>>();

        public SiteMap() { }

        /// <summary>
        /// Adds new page to site map graph
        /// </summary>
        /// <param name="url">Page url</param>
        /// <returns>Adding result</returns>
        public bool AddPage(string url)
        {
            if (AdjacencyList.ContainsKey(url))
            {
                return false;
            }

            AdjacencyList[url] = new ConcurrentSet<string>();
            return true;
        }

        /// <summary>
        /// Adds new connection between two pages
        /// </summary>
        /// <param name="link">Connection</param>
        /// <returns>Adding result</returns>
        public bool AddLink(Tuple<string, string> link)
        {
            if (AdjacencyList.ContainsKey(link.Item1) && AdjacencyList.ContainsKey(link.Item2))
            {
                return (AdjacencyList[link.Item1].TryAdd(link.Item2) & AdjacencyList[link.Item2].TryAdd(link.Item1));
            }

            return false;
        }
    }
}
