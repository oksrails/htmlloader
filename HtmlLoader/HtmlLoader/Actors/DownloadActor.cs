﻿using Akka.Actor;
using HtmlAgilityPack;
using HtmlLoader.Utils;
using static HtmlLoader.Models.Messages;

namespace HtmlLoader.Actors
{
    /// <summary>
    /// Actor responsible for download html page
    /// </summary>
    public class DownloadActor : ReceiveActor
    {
        private DirectoryInfo _directoryInfo;
        private string _url;
        private HtmlWeb _htmlWeb;

        public static Props GetProps(string url, DirectoryInfo directoryInfo)
            => Props.Create(() => new DownloadActor(url, directoryInfo));

        public DownloadActor(string url, DirectoryInfo directoryInfo)
        {
            _directoryInfo = directoryInfo;
            _url = url;
            _htmlWeb = new HtmlWeb();

            Process();
        }

        private void Process()
        {
            Receive<Start>(start =>
            {
                var htmlDocument = _htmlWeb.Load(_url);

                string filePath = DirectoryHelpers.BuildHtmlFilePath(_directoryInfo);

                var fileStream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                htmlDocument.Save(fileStream);
                fileStream.Close();

                Sender.Tell(new Done(_url));
            });
        }
    }
}
