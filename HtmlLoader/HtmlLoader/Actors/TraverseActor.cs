﻿using Akka.Actor;
using HtmlAgilityPack;
using HtmlLoader.Models;
using HtmlLoader.Utils;
using System.Collections.Concurrent;
using static HtmlLoader.Models.Messages;

namespace HtmlLoader.Actors
{
    /// <summary>
    /// Actor responsible for creating site map and orhestrating download actors
    /// </summary>
    public class TraverseActor : ReceiveActor
    {
        private string _baseUrl;
        private DirectoryInfo _directoryInfo;

        private SiteMap _siteMap;
        private HtmlWeb _htmlWeb;

        private ConcurrentDictionary<string, DirectoryInfo> _directories = new ConcurrentDictionary<string, DirectoryInfo>();
        private Dictionary<string, IActorRef> _childActors = new Dictionary<string, IActorRef>();

        private int _total = 0;
        private int _done = 0;

        public static Props GetProps(string baseUrl, DirectoryInfo directoryInfo)
            => Props.Create(() => new TraverseActor(baseUrl, directoryInfo));

        public TraverseActor(string baseUrl, DirectoryInfo directoryInfo)
        {
            _baseUrl = baseUrl;
            _directoryInfo = directoryInfo;
            _siteMap = new SiteMap();
            _htmlWeb = new HtmlWeb();

            Process();
        }

        private void Process()
        {
            Receive<Start>(start =>
            {
                Console.WriteLine("Creating site map...");

                _siteMap.AddPage(_baseUrl);
                _directories.TryAdd(_baseUrl, _directoryInfo);
                AddMapElement(_baseUrl);

                Console.WriteLine("Site map is finished.");

                CreateAndStartChildren();

                Console.WriteLine("Download started.");
            });

            Receive<Done>(done =>
            {
                _childActors.Remove(done.Url);
                _done++;

                Sender.Tell(PoisonPill.Instance);

                ConsoleUtility.RefreshProgress(_total, _done);

                if (!_childActors.Any())
                    Console.WriteLine(Environment.NewLine + "Download complete.");
            });
        }

        /// <summary>
        /// Recursively adds new pages and links between them
        /// </summary>
        /// <param name="url"></param>
        private void AddMapElement(string url)
        {
            var htmlDocument = _htmlWeb.Load(url);
            if (htmlDocument is null)
            {
                Console.WriteLine("No html document found at given address {0}", url);
                return;
            }

            var nodes = htmlDocument.DocumentNode.SelectNodes("//a[@href]");

            if (nodes is not null)
            {
                Parallel.ForEach(nodes, node =>
                {
                    string href = node.Attributes["href"].Value.ToString();
                    if (HtmlHelpers.ValidateHref(href))
                    {
                        var suburl = _baseUrl + href;
                        var isAdded = _siteMap.AddPage(suburl);
                        _siteMap.AddLink(new Tuple<string, string>(url, suburl));

                        var subdirectory = _directoryInfo.CreateSubdirectories(href);
                        _directories.TryAdd(suburl, subdirectory);

                        if (isAdded)
                            AddMapElement(suburl);
                    }
                });
            }
        }

        /// <summary>
        /// Create download actors according to pages and their directories
        /// </summary>
        private void CreateAndStartChildren()
        {
            foreach (var dir in _directories)
            {
                var child = Context.ActorOf(DownloadActor.GetProps(dir.Key, dir.Value));
                _childActors.Add(dir.Key, child);
                child.Tell(Start.Instance);
            }
            _total = _childActors.Count;
        }
    }
}
